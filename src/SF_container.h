/**
* @file SF_container.h
* @brief Basic containers.
* @author Aurel Neic
* @version
* @date 2017-02-14
*/

#ifndef _SF_CONTAINER_H
#define _SF_CONTAINER_H

#include <set>
#include "hashmap.hpp"

namespace SF {

/// element type enum
enum elem_t
{
  Tetra,
  Hexa,
  Octa,
  Pyramid,
  Prism,
  Quad,
  Tri,
  Line
};

/// Point and vector struct
struct Point {
  double x;
  double y;
  double z;
};

inline Point arr_to_point(double* arr)
{
  return {arr[0], arr[1], arr[2]};
}

inline void point_to_arr(Point & p, double* arr)
{
  arr[0] = p.x, arr[1] = p.y, arr[2] = p.z;
}

/// cross product
inline Point cross(const Point & a, const Point & b)
{
  Point  c = {a.y*b.z - b.y*a.z, b.x*a.z - a.x*b.z, a.x*b.y - a.y*b.x};
  return c;
}

inline double inner_prod(const Point & a, const Point & b)
{
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline void outer_prod(const Point & a, const Point & b, const double s, double* buff, const bool add = false)
{
  if(add == false)
    memset(buff, 0, 9*sizeof(double));

  buff[0] += a.x*b.x*s, buff[1] += a.x*b.y*s, buff[2] += a.x*b.z*s;
  buff[3] += a.y*b.x*s, buff[4] += a.y*b.y*s, buff[5] += a.y*b.z*s;
  buff[6] += a.z*b.x*s, buff[7] += a.z*b.y*s, buff[8] += a.z*b.z*s;
}

inline void outer_prod(const Point & a, const Point & b, double* buff)
{
  outer_prod(a, b, 1.0, buff);
}

/// vector magnitude
inline double mag(const Point & vect)
{
  return sqrt(inner_prod(vect, vect));
}

inline Point normalize(const Point & vect)
{
  double norm = mag(vect);
  return {vect.x / norm, vect.y / norm, vect.z / norm};
}

inline Point operator-(const Point & a, const Point & b)
{
  return {a.x - b.x, a.y - b.y, a.z - b.z};
}

inline Point operator+(const Point & a, const Point & b)
{
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

inline Point operator*(const Point & a, const double & s)
{
  return {a.x * s, a.y * s, a.z * s};
}

/// project b onto a
inline Point project(const Point & a, const Point & b)
{
  return a * inner_prod(normalize(a), normalize(b));
}

inline Point orthogonalize(const Point & a, const Point & b)
{
  return a - project(a, b);
}


inline double distance(const Point & a, const Point & b)
{
  return mag(a - b);
}

/// Generate element type enum from string
inline elem_t getElemTypeID(char *eletype)
{
  elem_t ret;

  if ( !strcmp( eletype, "Tt" ) ) {
    ret = Tetra;
  } else if ( !strcmp( eletype, "Hx" ) ) {
    ret = Hexa;
  } else if ( !strcmp( eletype, "Oc" ) ) {
    ret = Octa;
  } else if ( !strcmp( eletype, "Py" ) ) {
    ret = Pyramid;
  } else if ( !strcmp( eletype, "Pr" ) ) {
    ret = Prism;
  } else if ( !strcmp( eletype, "Qd" ) ) {
    ret = Quad;
  } else if ( !strcmp( eletype, "Tr" ) ) {
    ret = Tri;
  } else {
    ret = Line;
  }
  return ret;
}

template<class S, class POINT> inline
void array_to_points(const vector<S> & arr, vector<POINT> & pts)
{
  pts.resize(arr.size() / 3);
  for(size_t i=0; i<pts.size(); i++)
    pts[i] = {arr[i*3+0], arr[i*3+1], arr[i*3+2]};
}

/// Enumeration encoding the different supported numberings
typedef enum {
  NBR_REF,           ///< The numbering of the reference mesh (the one stored on HD).
  NBR_SUBMESH,       ///< Submesh numbering: The globally ascending sorted reference indices are reindexed.
  NBR_PETSC,         ///< A mesh or submesh renumbered for PETSc.
  NBR_SOLVER         ///< A used solver may have its own numbering
} SF_nbr;

// forward declaration of parallel layouts since they are used in mesh.
template<class T> class overlapping_layout;
template<class T> class non_overlapping_layout;

/**
 * \brief The mesh storage class. It contains both element and vertex data.
 */
template<class T, class S>
class meshdata
{
public:
  size_t g_numelem; ///< global number of elements
  size_t l_numelem; ///< local  number of elements
  size_t g_numpts;  ///< global number of points
  size_t l_numpts;  ///< local  number of points

  /// the parallel mesh is defined on a MPI world
  MPI_Comm comm;

  /// the mesh name
  std::string name;

  // element connectivity : the nodes forming the elements
  vector<T> con; ///< connectivity in local indexing

  // element data
  // one value per element
  vector<T> dsp;       ///< connectivity starting index of each element
  vector<T> tag;       ///< element tag
  vector<elem_t> type; ///< element type
  // multiple values per element
  vector<S> fib;       ///< fiber direction
  vector<S> she;       ///< sheet direction

  vector<T>  ref_eidx; ///< element index in reference mesh
  /// the element tags based on which the mesh has been extracted
  hashmap::unordered_set<int> extr_tag;

  std::map< SF_nbr, vector<T> > nbr;   ///< container for different numberings
  vector<S> xyz;                       ///< node cooridnates

  overlapping_layout<T> pl;        ///< nodal parallel layout
  non_overlapping_layout<T> epl;   ///< element parallel layout

  /// construct empty mesh
  meshdata(): g_numelem(0), l_numelem(0), g_numpts(0), l_numpts(0),
             comm(SF_COMM), name("unnamed")
  {}

  /**
  * @brief Register a new numbering to the mesh and return the associated index vector
  *
  * @param nbr_type  The type of numbering
  *
  * @return          The index vector holding the indices of the numbering.
  */
  inline vector<T> & register_numbering(SF_nbr nbr_type)
  {
    if(nbr.count(nbr_type) == 0)
      nbr[nbr_type] = vector<T>();

    return nbr[nbr_type];
  }

  /**
  * @brief Get the vector defining a certain numbering.
  *
  * Note: If the requested numbering does not exist, this function will abort with an assertion.
  * This is because a requested but not existent numbering hints a severe error in the logic of
  * the calling code.
  * If you want to check if a numbering exists, use: mesh::nbr.count(SF_nbr type)
  *
  * @param nbr_type  The requested numbering type.
  *
  * @return A reference of the requested numbering.
  */
  inline vector<T> & get_numbering(SF_nbr nbr_type)
  {
    typename std::map< SF_nbr, vector<T> >::iterator it = nbr.find(nbr_type);
    assert(it != nbr.end());
    return it->second;
  }
  /**
  * @brief Get the vector defining a certain numbering.
  *
  * Note: If the requested numbering does not exist, this function will abort with an assertion.
  * This is because a requested but not existent numbering hints a severe error in the logic of
  * the calling code.
  * If you want to check if a numbering exists, use: mesh::nbr.count(SF_nbr type)
  *
  * @param nbr_type  The requested numbering type.
  *
  * @return A const reference of the requested numbering.
  */
  inline const vector<T> & get_numbering(SF_nbr nbr_type) const
  {
    typename std::map< SF_nbr, vector<T> >::const_iterator it = nbr.find(nbr_type);
    assert(it != nbr.end());
    return it->second;
  }

  /**
  * @brief Localize the connectivity data w.r.t. a given numbering.
  *
  * The call expects the connectivity indices to be globalized.
  *
  * @param nbr_type  The numbering.
  */
  inline void localize(SF_nbr nbr_type)
  {
    // register numbering
    vector<T> & nod = this->register_numbering(nbr_type);

    hashmap::unordered_map<T,T> g2l;
    for(const T & c : this->con) g2l[c] = 0;
    g2l.sort();

    // compute numbering
    nod.resize(g2l.size());

    size_t widx = 0;
    for(auto it = g2l.begin(); it != g2l.end(); ++it, widx++) {
      nod[widx] = it->first;
      it->second = widx;
    }

    global_to_local(g2l, this->con, true);
    this->l_numpts = nod.size();
  }

  /**
   *  \brief Set up the parallel layout.
   *
   *  \post The overlapping_layout member class has been set up
   */
  inline void generate_par_layout()
  {
    const vector<T> & rnod = this->get_numbering(NBR_REF);
    pl.assign(rnod, comm);
    g_numpts = pl.num_global_idx();

    epl.assign(ref_eidx, comm);
  }

  /**
  * @brief Clear the mesh data from memory
  */
  inline void clear_data()
  {
    g_numelem = 0;
    g_numpts = 0;
    l_numelem = 0;
    l_numpts = 0;

    con.resize(0); con.reallocate();
    dsp.resize(0); dsp.reallocate();
    tag.resize(0); tag.reallocate();
    type.resize(0); type.reallocate();

    fib.resize(0); fib.reallocate();
    she.resize(0); she.reallocate();

    ref_eidx.resize(0); ref_eidx.reallocate();
    xyz.resize(0); xyz.reallocate();

    extr_tag.clear();
    nbr.clear();
  }
};

template<class T, class S>
void get_alg_mask(const meshdata<T,S> & mesh, vector<bool> & alg_mask)
{
  const vector<T> & alg_nod = mesh.pl.algebraic_nodes();

  alg_mask.assign(mesh.l_numpts, false);
  for(const T & n : alg_nod) alg_mask[n] = true;
}


/**
 *  \brief The class holds the communication graph for a MPI_Exchange() call.
 *
 */
template<class T>
class commgraph
{
  public:
  vector<T> scnt; ///< Number of elements sent to each rank.
  vector<T> sdsp; ///< Displacements w.r.t. scnt.
  vector<T> rcnt; ///< Number of elements received from each rank.
  vector<T> rdsp; ///< Displacements w.r.t. rcnt.

  /// Resize all vectors to size.
  inline void resize(size_t size)
  {
    scnt.resize(size), rcnt.resize(size), sdsp.resize(size+1), rdsp.resize(size+1);
  }

  /// scale comm graph layout data
  template<class V>
  inline void scale(V fac)
  {
    for(size_t i=0; i<scnt.size(); i++)
      scnt[i] *= fac, sdsp[i] *= fac, rcnt[i] *= fac, rdsp[i] *= fac;
  }

  /// transpose comm graph (receive becomes send, and vice versa)
  inline void transpose()
  {
    vector<T> tcnt(scnt), tdsp(sdsp);
    scnt = rcnt, sdsp = rdsp;
    rcnt = tcnt, rdsp = tdsp;
  }

  /**
   * \brief Set up the communication graph.
   *
   * \param [in] dest  The destination of each local element to communicate.
   * \param [in] comm  The MPI_Comm used for the communcation.
   *
   * \post  All vectors in the class have been configured.
   */
  template<class V>
  inline void configure(const vector<V> & dest, MPI_Comm comm)
  {
    int size, rank;
    MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

    this->resize(size);

    count(dest, scnt);
    MPI_Alltoall(scnt.data(), sizeof(T), MPI_BYTE, rcnt.data(), sizeof(T), MPI_BYTE, comm);
    dsp_from_cnt(scnt, sdsp);
    dsp_from_cnt(rcnt, rdsp);
  }
  /**
   * \brief Set up the communication graph.
   *
   * \param [in] dest  The destination of each logical element to communicate.
   * \param [in] cnt   The number of entries of each logical element.
   * \param [in] comm  The MPI_Comm used for the communcation.
   *
   * \post  All vectors in the class have been configured.
   */
  template<class V>
  inline void configure(const vector<V> & dest, const vector<T> & cnt, MPI_Comm comm)
  {
    int size, rank;
    MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

    this->resize(size);

    scnt.zero();
    for(size_t i=0; i<dest.size(); i++) scnt[dest[i]] += cnt[i];

    MPI_Alltoall(scnt.data(), sizeof(T), MPI_BYTE, rcnt.data(), sizeof(T), MPI_BYTE, comm);

    dsp_from_cnt(scnt, sdsp);
    dsp_from_cnt(rcnt, rdsp);
  }
  /**
   * \brief For every received data element, get the rank indices it was receive from.
   *
   * Note: The commgraph class needs to be configured in order for this function
   * to produce a meaningful result.
   *
   * \param [out] source  A vector holding the source rank index to every element of data received.
   *
   */
  template<class V>
  inline void source_ranks(vector<V> & source)
  {
    size_t size = rcnt.size();
    source.resize(sum(rcnt));

    for(size_t i=0, widx=0; i<size; i++)
      for(T j=0; j<rcnt[i]; j++, widx++)
        source[widx] = i;
  }
};

template<class T>
struct tuple {
  T v1;
  T v2;
};

template<class T, class S>
struct mixed_tuple
{
  T v1;
  S v2;
};

template<class T>
struct triple {
  T v1;
  T v2;
  T v3;
};

template<class T, class S, class V>
struct mixed_triple {
  T v1;
  S v2;
  V v3;
};

template<class T>
struct quadruple {
  T v1;
  T v2;
  T v3;
  T v4;
};

template<class T>
bool operator<(const struct tuple<T> & lhs, const struct tuple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else
    return lhs.v2 < rhs.v2;
}
template<class T, class S>
bool operator<(const struct mixed_tuple<T, S> & lhs, const struct mixed_tuple<T, S> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else
    return lhs.v2 < rhs.v2;
}

template<class T>
bool operator<(const struct triple<T> & lhs, const struct triple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else if(lhs.v2 != rhs.v2)
    return lhs.v2 < rhs.v2;
  else
    return lhs.v3 < rhs.v3;
}

template<class T>
bool operator<(const struct quadruple<T> & lhs, const struct quadruple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else if(lhs.v2 != rhs.v2)
    return lhs.v2 < rhs.v2;
  else if(lhs.v3 != rhs.v3)
    return lhs.v3 < rhs.v3;
  else
    return lhs.v4 < rhs.v4;
}

// expand hashing for custom triple struct

}

namespace hashmap {

template<typename T>
struct hash_ops< SF::tuple<T> >
{
  static inline
  bool cmp(SF::tuple<T> a, SF::tuple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2;
  }

  static inline
  hm_uint hash(SF::tuple<T> a) {
    hm_uint h = hashmap::mkhash_init;
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v1));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v2));
    return h;
  }
};

template<typename T>
struct hash_ops< SF::triple<T> >
{
  static inline
  bool cmp(SF::triple<T> a, SF::triple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2 && a.v3 == b.v3;
  }

  static inline
  hm_uint hash(SF::triple<T> a) {
    hm_uint h = mkhash_init;
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v1));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v2));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v3));
    return h;
  }
};

template<typename T>
struct hash_ops< SF::quadruple<T> >
{
  static inline
  bool cmp(SF::quadruple<T> a, SF::quadruple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2 && a.v3 == b.v3 && a.v4 == b.v4;
  }

  static inline
  hm_uint hash(SF::quadruple<T> a) {
    hm_uint h = mkhash_init;
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v1));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v2));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v3));
    h = hashmap::mkhash(h, hashmap::hash_ops<T>::hash(a.v4));
    return h;
  }
};
}

template<class T>
struct tri_sele {
  T v1;
  T v2;
  T v3;
  T eidx;
};

template<class T>
struct quad_sele {
  T v1;
  T v2;
  T v3;
  T v4;
  T eidx;
};

/// sort the "in" triple into the "out" triple
template<class T>
void sort_triple(const T in1, const T in2, const T in3, T & out1, T & out2, T & out3)
{
    bool t12 = in1 < in2;
  bool t13 = in1 < in3;
  bool t23 = in2 < in3;

  if(t12) {
    //(123),(312),(132)
    if(t23) {
      //123
      out1=in1; out2=in2; out3=in3;
    }
    else {
      //(312),(132)
      if(t13) {
        //132
        out1=in1; out2=in3; out3=in2;
      }
      else {
        //312
        out1=in3; out2=in1; out3=in2;
      }
    }
  }
  else {
    //(213),(231),(321)
    if(t23) {
      //(213),(231)
      if(t13) {
        //213
        out1=in2; out2=in1; out3=in3;
      }
      else {
        //231
        out1=in2; out2=in3; out3=in1;
      }
    }
    else {
      //321
      out1=in3; out2=in2; out3=in1;
    }
  }
}

#endif

