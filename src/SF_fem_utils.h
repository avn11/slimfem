/**
* @file SF_fem_utils.h
* @brief FEM utilities.
*
* Some of this funcitons (num dof, integration points, shape
* funcs) have been copied over from the carpentry source base.
*
* @author Aurel Neic, Christoph Augustin
* @version
* @date 2017-11-15
*/


#ifndef _SF_FEM_H
#define _SF_FEM_H

#include<cassert>

#define SF_MAX_ELEM_NODES 10  //!< max \#nodes defining an element

namespace SF {

/**
* @brief Get number of d.o.f. for an element typa and an Ansatz function order.
*
* @param type   Element type.
* @param order  Ansatz function order
*
* @return Number of d.o.f.
*/
inline short num_dof(elem_t type, short order)
{
  short ndof = -1;
  switch(type) {
    case Line:
      if (order == 1)      ndof = 2;
      else if (order == 2) ndof = 3;
      else {
        fprintf(stderr, "Line element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Tri:
      if (order == 1)      ndof = 3;
      else if (order == 2) ndof = 6;
      else {
        fprintf(stderr, "Tri element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Quad:
      if (order == 1)      ndof = 4;
      else if (order == 2) ndof = 8;
      else {
        fprintf(stderr, "Quad element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Tetra:
      if (order == 1)      ndof = 4;
      else if (order == 2) ndof = 10;
      else {
        fprintf(stderr, "Tetra element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Hexa:
      if (order == 1)      ndof = 8;
      else if (order == 2) ndof = 20; // serendipity element
      else {
        fprintf(stderr, "Hexa element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;

    default:
        fprintf(stderr, "SF::num_dof: Unsupported element type.\n");
        exit(1);
  }

  return ndof;
}


/**
* @brief Compute the integration point locations and weights.
*
* @param type   Element type.
* @param order  Integration order.
* @param ip     Integration point locations.
* @param w      Integration weights.
* @param nint   Number of integration points.
*/
inline void general_integration_points(const elem_t type,
                                const short order,
                                Point* ip,
                                double* w,
                                int & nint)
{
  switch (type)
  {
    case Line:
      if (order == 1)
      {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 1.; nint = 1;
        return;
      }
      if (order == 2)
      {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = 0.; ip[0].z = 0.;
        ip[1].x = +sqrt3; ip[1].y = 0.; ip[1].z = 0.;
        w[0] = 1.; w[1] = 1.; nint = 2;
        return;
      }
      break;

    case Tri:
      if (order == 1)
      {
        ip[0].x = 1. / 3.; ip[0].y = 1. / 3.; ip[0].z = 0.;
        w[0] = 1. / 2.;
        nint = 1;
        return;
      }
      if (order == 2)
      {
        ip[0].x = 1. / 6.; ip[0].y = 1. / 6.; ip[0].z = 0.;
        ip[1].x = 4. / 6.; ip[1].y = 1. / 6.; ip[1].z = 0.;
        ip[2].x = 1. / 6.; ip[2].y = 4. / 6.; ip[2].z = 0.;
        w[0] = 1. / 6.; w[1] = 1. / 6.; w[2] = 1. / 6.;
        nint = 3;
        return;
      }
      if (order == 3 || order == 4) // Gauss computed
      {
        ip[0].x = 0.188409405952072339650, ip[0].y = 0.787659461760847001700, ip[0].z = 0;
        ip[1].x = 0.523979067720100721850, ip[1].y = 0.409466864440734712450, ip[1].z = 0;
        ip[2].x = 0.808694385677669824730, ip[2].y = 0.088587959512703928766, ip[2].z = 0;
        ip[3].x = 0.106170269119576471390, ip[3].y = 0.787659461760847001700, ip[3].z = 0;
        ip[4].x = 0.295266567779632616020, ip[4].y = 0.409466864440734712450, ip[4].z = 0;
        ip[5].x = 0.455706020243648035620, ip[5].y = 0.088587959512703928766, ip[5].z = 0;
        ip[6].x = 0.023931132287080617016, ip[6].y = 0.787659461760847001700, ip[6].z = 0;
        ip[7].x = 0.066554067839164496312, ip[7].y = 0.409466864440734712450, ip[7].z = 0;
        ip[8].x = 0.102717654809626260380, ip[8].y = 0.088587959512703928766, ip[8].z = 0;

        w[0] = 0.019396383305959434551;
        w[1] = 0.063678085099884929043;
        w[2] = 0.055814420483044288601;
        w[3] = 0.03103421328953510222;
        w[4] = 0.1018849361598159059;
        w[5] = 0.089303072772870889517;
        w[6] = 0.019396383305959434551;
        w[7] = 0.063678085099884929043;
        w[8] = 0.055814420483044288601;
        nint = 9;
        return;
      }
      if (order == 5)
      {
        double sixmsqrt15  = 0.10128650732345633880; // (6-sqrt(15))/21
        double sixpsqrt15  = 0.47014206410511508977; // (6+sqrt(15))/21
        double ninepsqrt15 = 0.79742698535308732239; // (9+2*sqrt(15))/21
        double ninemsqrt15 = 0.05971587178976982046; // (9-2*sqrt(15))/21
        double one3        = 0.33333333333333333333; // 1/3
        ip[0].x = sixmsqrt15; ip[0].y = sixmsqrt15; ip[0].z = 0.;
        ip[1].x = ninepsqrt15; ip[1].y = sixmsqrt15; ip[1].z = 0.;
        ip[2].x = sixmsqrt15; ip[2].y = ninepsqrt15; ip[2].z = 0.;
        ip[3].x = sixpsqrt15; ip[3].y = sixpsqrt15; ip[3].z = 0.;
        ip[4].x = ninemsqrt15; ip[4].y = sixpsqrt15; ip[4].z = 0.;
        ip[5].x = sixpsqrt15; ip[5].y = ninemsqrt15; ip[5].z = 0.;
        ip[6].x = one3; ip[6].y = one3; ip[6].z = 0.;

        double weight1 = 0.06296959027241357629; //(155-sqrt(15))/2400
        double weight2 = 0.06619707639425309036; //(155+sqrt(15))/2400
        double weight3 = 0.1125;               // 9/80

        w[0] = weight1;
        w[3] = weight2;
        w[6] = weight3;
        w[1] = weight1;
        w[4] = weight2;
        w[2] = weight1;
        w[5] = weight2;

        nint = 7;
        return;
      }
      break;

    case Quad:
      if (order == 1)
      {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 4.;
        nint = 1;
        return;
      }
      if (order == 2 || order == 3)
      {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = -sqrt3; ip[0].z = 0.;
        ip[1].x = +sqrt3; ip[1].y = -sqrt3; ip[1].z = 0.;
        ip[2].x = +sqrt3; ip[2].y = +sqrt3; ip[2].z = 0.;
        ip[3].x = -sqrt3; ip[3].y = +sqrt3; ip[3].z = 0.;
        w[0] = 1.; w[1] = 1.; w[2] = 1.; w[3] = 1.;
        nint = 4;
        return;
      }
      if (order == 4)   // Gauss computed
      {
        ip[0].x = 0.88729833462074170214, ip[0].y = 0.88729833462074170214, ip[0].z = 0;
        ip[1].x = 0.88729833462074170214, ip[1].y = 0.5,                    ip[1].z = 0;
        ip[2].x = 0.88729833462074170214, ip[2].y = 0.11270166537925829786, ip[2].z = 0;
        ip[3].x = 0.5,                    ip[3].y = 0.88729833462074170214, ip[3].z = 0;
        ip[4].x = 0.5,                    ip[4].y = 0.5,                    ip[4].z = 0;
        ip[5].x = 0.5,                    ip[5].y = 0.11270166537925829786, ip[5].z = 0;
        ip[6].x = 0.11270166537925829786, ip[6].y = 0.88729833462074170214, ip[6].z = 0;
        ip[7].x = 0.11270166537925829786, ip[7].y = 0.5,                    ip[7].z = 0;
        ip[8].x = 0.11270166537925829786, ip[8].y = 0.11270166537925829786, ip[8].z = 0;

        w[0] = 0.077160493827160225866;
        w[1] = 0.12345679012345638081;
        w[2] = 0.077160493827160225866;
        w[3] = 0.12345679012345638081;
        w[4] = 0.19753086419753024261;
        w[5] = 0.12345679012345638081;
        w[6] = 0.077160493827160225866;
        w[7] = 0.12345679012345638081;
        w[8] = 0.077160493827160225866;

        nint = 9;
        return;
      }
      break;

    case Tetra:
      if (order == 0 || order == 1) // exact for linears
      {
        ip[0].x = 0.25; ip[0].y = 0.25; ip[0].z = 0.25;
        w[0] = .16666666666666666666;
        nint = 1;
      }
      if (order == 2) // exact for quadratics
      {
        double gauss1 = 0.13819660112501051518; // (5-sqrt(5))/20
        double gauss2 = 0.58541019662496845446; // (5+3sqrt(5))/20
        double weight = 0.0416666666666666666666666666666666; // 1/24
        ip[0].x = gauss1; ip[0].y = gauss1; ip[0].z = gauss1;
        ip[1].x = gauss2; ip[1].y = gauss1; ip[1].z = gauss1;
        ip[2].x = gauss1; ip[2].y = gauss2; ip[2].z = gauss1;
        ip[3].x = gauss1; ip[3].y = gauss1; ip[3].z = gauss2;
        w[0] = weight; w[1] = weight; w[2] = weight; w[3] = weight;
        nint = 4;
        return;
      }
      // Gauss computed
      if (order == 3 || order == 4)
      {
        ip[0].x = 0.12764656212038541505,  ip[0].y = 0.29399880063162286969,  ip[0].z = 0.54415184401122529412;
        ip[1].x = 0.2457133252117133515,   ip[1].y = 0.56593316507280100325,  ip[1].z = 0.12251482265544139105;
        ip[2].x = 0.30377276481470755209,  ip[2].y = 0.070679724159396897787, ip[2].z = 0.54415184401122529412;
        ip[3].x = 0.58474756320489440498,  ip[3].y = 0.13605497680284600603,  ip[3].z = 0.12251482265544139105;
        ip[4].x = 0.034202793236766414198, ip[4].y = 0.29399880063162286969,  ip[4].z = 0.54415184401122529412;
        ip[5].x = 0.065838687060044420729, ip[5].y = 0.56593316507280100325,  ip[5].z = 0.12251482265544139105;
        ip[6].x = 0.081395667014670256001, ip[6].y = 0.070679724159396897787, ip[6].z = 0.54415184401122529412;
        ip[7].x = 0.15668263733681833672,  ip[7].y = 0.13605497680284600603,  ip[7].z = 0.12251482265544139105;

        w[0] = 0.0091694299214797256314;
        w[1] = 0.0211570064545240181520;
        w[2] = 0.0160270405984766287080;
        w[3] = 0.0369798563588529458080;
        w[4] = 0.0091694299214797291009;
        w[5] = 0.0211570064545240285600;
        w[6] = 0.0160270405984766356470;
        w[7] = 0.0369798563588529666250;

        nint = 8;
        return;
      }
      // Walkington "Quadrature on simplices of arbitrary dimension"
      if (order == 5)  // || order == 4 || order == 3)
      {
        double a = 0.31088591926330060980;
        double b = 1. - 3.*a;
        double c = 0.092735250310891226402;
        double d = 1. - 3.*c;
        //double e = 0.4544963;
        double e = 0.045503704125649649492;
        double f = 0.5 - e;

        ip[0].x = a; ip[0].y = a; ip[0].z = a;
        ip[1].x = b; ip[1].y = a; ip[1].z = a;
        ip[2].x = a; ip[2].y = b; ip[2].z = a;
        ip[3].x = a; ip[3].y = a; ip[3].z = b;

        ip[4].x = c; ip[4].y = c; ip[4].z = c;
        ip[5].x = d; ip[5].y = c; ip[5].z = c;
        ip[6].x = c; ip[6].y = d; ip[6].z = c;
        ip[7].x = c; ip[7].y = c; ip[7].z = d;

        ip[8].x  = f; ip[8].y  = e; ip[8].z  = e;
        ip[9].x  = e; ip[9].y  = f; ip[9].z  = e;
        ip[10].x = e; ip[10].y = e; ip[10].z = f;
        ip[11].x = e; ip[11].y = f; ip[11].z = f;
        ip[12].x = f; ip[12].y = e; ip[12].z = f;
        ip[13].x = f; ip[13].y = f; ip[13].z = e;

        double w1 = 0.018781320953002641800;
        double w2 = 0.012248840519393658257;
        double w3 = 0.007091003462846911073;

        w[0] = w1; w[1] = w1; w[2] = w1; w[3] = w1;
        w[4] = w2; w[5] = w2; w[6] = w2; w[7] = w2;
        w[8] = w3; w[9] = w3; w[10] = w3; w[11] = w3;
        w[12] = w3; w[13] = w3;

        nint = 14;
        return;
      }
      break;

    case Hexa:
      if (order == 0)
      {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 8.;
        nint = 1;
        return;
      }
      if (order == 1 || order == 2)
      {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = -sqrt3; ip[0].z = -sqrt3;
        ip[1].x = +sqrt3; ip[1].y = -sqrt3; ip[1].z = -sqrt3;
        ip[2].x = +sqrt3; ip[2].y = +sqrt3; ip[2].z = -sqrt3;
        ip[3].x = -sqrt3; ip[3].y = +sqrt3; ip[3].z = -sqrt3;
        ip[4].x = -sqrt3; ip[4].y = -sqrt3; ip[4].z = +sqrt3;
        ip[5].x = +sqrt3; ip[5].y = -sqrt3; ip[5].z = +sqrt3;
        ip[6].x = +sqrt3; ip[6].y = +sqrt3; ip[6].z = +sqrt3;
        ip[7].x = -sqrt3; ip[7].y = +sqrt3; ip[7].z = +sqrt3;
        w[0] = 1.; w[1] = 1.; w[2] = 1.; w[3] = 1.;
        w[4] = 1.; w[5] = 1.; w[6] = 1.; w[7] = 1.;
        nint = 8;
        return;
      }
      // Gauss quadrature rule computed
      if (order == 4)
      {
        ip[0].x = 0.88729833462074170214, ip[0].y = 0.88729833462074170214, ip[0].z = 0.88729833462074170214;
        ip[1].x = 0.88729833462074170214, ip[1].y = 0.88729833462074170214, ip[1].z = 0.5;
        ip[2].x = 0.88729833462074170214, ip[2].y = 0.88729833462074170214, ip[2].z = 0.11270166537925829786;
        ip[3].x = 0.88729833462074170214, ip[3].y = 0.5, ip[3].z = 0.88729833462074170214;
        ip[4].x = 0.88729833462074170214, ip[4].y = 0.5, ip[4].z = 0.5;
        ip[5].x = 0.88729833462074170214, ip[5].y = 0.5, ip[5].z = 0.11270166537925829786;
        ip[6].x = 0.88729833462074170214, ip[6].y = 0.11270166537925829786, ip[6].z = 0.88729833462074170214;
        ip[7].x = 0.88729833462074170214, ip[7].y = 0.11270166537925829786, ip[7].z = 0.5;
        ip[8].x = 0.88729833462074170214, ip[8].y = 0.11270166537925829786, ip[8].z = 0.11270166537925829786;
        ip[9].x = 0.5, ip[9].y = 0.88729833462074170214, ip[9].z = 0.88729833462074170214;
        ip[10].x = 0.5, ip[10].y = 0.88729833462074170214, ip[10].z = 0.5;
        ip[11].x = 0.5, ip[11].y = 0.88729833462074170214, ip[11].z = 0.11270166537925829786;
        ip[12].x = 0.5, ip[12].y = 0.5, ip[12].z = 0.88729833462074170214;
        ip[13].x = 0.5, ip[13].y = 0.5, ip[13].z = 0.5;
        ip[14].x = 0.5, ip[14].y = 0.5, ip[14].z = 0.11270166537925829786;
        ip[15].x = 0.5, ip[15].y = 0.11270166537925829786, ip[15].z = 0.88729833462074170214;
        ip[16].x = 0.5, ip[16].y = 0.11270166537925829786, ip[16].z = 0.5;
        ip[17].x = 0.5, ip[17].y = 0.11270166537925829786, ip[17].z = 0.11270166537925829786;
        ip[18].x = 0.11270166537925829786, ip[18].y = 0.88729833462074170214, ip[18].z = 0.88729833462074170214;
        ip[19].x = 0.11270166537925829786, ip[19].y = 0.88729833462074170214, ip[19].z = 0.5;
        ip[20].x = 0.11270166537925829786, ip[20].y = 0.88729833462074170214, ip[20].z = 0.11270166537925829786;
        ip[21].x = 0.11270166537925829786, ip[21].y = 0.5, ip[21].z = 0.88729833462074170214;
        ip[22].x = 0.11270166537925829786, ip[22].y = 0.5, ip[22].z = 0.5;
        ip[23].x = 0.11270166537925829786, ip[23].y = 0.5, ip[23].z = 0.11270166537925829786;
        ip[24].x = 0.11270166537925829786, ip[24].y = 0.11270166537925829786, ip[24].z = 0.88729833462074170214;
        ip[25].x = 0.11270166537925829786, ip[25].y = 0.11270166537925829786, ip[25].z = 0.5;
        ip[26].x = 0.11270166537925829786, ip[26].y = 0.11270166537925829786, ip[26].z = 0.11270166537925829786;

        //weights
        w[0] = 0.0214334705075444678650;
        w[1] = 0.0342935528120711582980;
        w[2] = 0.0214334705075444678650;
        w[3] = 0.0342935528120711582980;
        w[4] = 0.0548696844993138629910;
        w[5] = 0.0342935528120711582980;
        w[6] = 0.0214334705075444678650;
        w[7] = 0.0342935528120711582980;
        w[8] = 0.0214334705075444678650;
        w[9] = 0.0342935528120711582980;
        w[10] = 0.054869684499313862991;
        w[11] = 0.034293552812071158298;
        w[12] = 0.054869684499313862991;
        w[13] = 0.087791495198902197439;
        w[14] = 0.054869684499313862991;
        w[15] = 0.034293552812071158298;
        w[16] = 0.054869684499313862991;
        w[17] = 0.034293552812071158298;
        w[18] = 0.021433470507544467865;
        w[19] = 0.034293552812071158298;
        w[20] = 0.021433470507544467865;
        w[21] = 0.034293552812071158298;
        w[22] = 0.054869684499313862991;
        w[23] = 0.034293552812071158298;
        w[24] = 0.021433470507544467865;
        w[25] = 0.034293552812071158298;
        w[26] = 0.021433470507544467865;

        nint = 27;
        return;
      }
      break;

    default: break;
  }
}


/**
* @brief Compute shape function and its derivatives on a reference element.
*
* @param type    Element type.
* @param order   Order of the Ansatz functions.
* @param ip      Integration point.
* @param rshape  Reference shape and deriv. matrix.
*/
inline void reference_shape(const elem_t type,
                        const int order,
                        const Point ip,
                        dmat<double> & rshape)
{
  switch (type) {
    case Line:
    {
      rshape[0][0] = 1.0 - ip.x;
      rshape[0][1] = ip.x;

      rshape[1][0] = -1.0;
      rshape[1][1] = 1.0;

      rshape[2][0] = 0.;
      rshape[2][1] = 0.;

      rshape[3][0] = 0.;
      rshape[3][1] = 0.;
      break;
    }

    case Tri:
    {
      double lam0 = (1.0 - ip.x - ip.y);
      double lam1 = ip.x;
      double lam2 = ip.y;

      rshape[0][0] = lam0;
      rshape[0][1] = lam1;
      rshape[0][2] = lam2;

      // derivative w.r.t. xi
      rshape[1][0] = -1.0;
      rshape[1][1] =  1.0;
      rshape[1][2] =  0.0;

      // derivative w.r.t. eta
      rshape[2][0] = -1.0;
      rshape[2][1] =  0.0;
      rshape[2][2] =  1.0;

      // derivative w.r.t. zeta (is zero)
      rshape[3][0] = 0.;
      rshape[3][1] = 0.;
      rshape[3][2] = 0.;

      if(order == 2) {
        rshape[0][3] = -2.*lam0 * lam1;
        rshape[0][4] = -2.*lam1 * lam2;
        rshape[0][5] = -2.*lam0 * lam2;

        rshape[1][3] = 2.*(lam0 - lam1);
        rshape[1][4] = -2.*lam2;
        rshape[1][5] = 2.*lam2;

        rshape[2][3] = 2.*lam1;
        rshape[2][4] = -2.*lam1;
        rshape[2][5] = 2.*(lam0 - lam2);

        rshape[3][3] = 0.;
        rshape[3][4] = 0.;
        rshape[3][5] = 0.;
      }
      break;
    }

    case Quad: {
      double qrtr = 0.25;
      const double node[4][2] =
      {
        { -1.0, -1.0},
        { 1.0, -1.0},
        { 1.0,  1.0},
        { -1.0,  1.0}
      };

      // adjust to ordering given in carp manual
      int v[4] = {0, 1, 2, 3};

      for (int i = 0; i < 4; i++) {
        // shape function
        rshape[0][i] = qrtr * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]);
        // derivative w.r.t. xi
        rshape[1][i] = node[v[i]][0] * qrtr * (1. + ip.y * node[v[i]][1]);
        // derivative w.r.t. eta
        rshape[2][i] = node[v[i]][1] * qrtr * (1. + ip.x * node[v[i]][0]);
        // derivative w.r.t. zeta (is zero)
        rshape[3][i] = 0.;
      }
      break;
    }

    case Tetra:
    {
      double lam0 = 1.0 - ip.x - ip.y - ip.z;
      double lam1 = ip.x;
      double lam2 = ip.y;
      double lam3 = ip.z;
      //static int v[10] = {0,3,1,2,7,8,4,6,9,5};

      // shape function
      rshape[0][0] = lam0;
      rshape[0][1] = lam1;
      rshape[0][2] = lam2;
      rshape[0][3] = lam3;

      // derivative w.r.t. xi
      rshape[1][0] = -1.0;
      rshape[1][1] =  1.0;
      rshape[1][2] =  0.0;
      rshape[1][3] =  0.0;

      // derivative w.r.t. eta
      rshape[2][0] = -1.0;
      rshape[2][1] =  0.0;
      rshape[2][2] =  1.0;
      rshape[2][3] =  0.0;

      // derivative w.r.t. zeta
      rshape[3][0] = -1.0;
      rshape[3][1] =  0.0;
      rshape[3][2] =  0.0;
      rshape[3][3] =  1.0;

      if (order == 2) {
        rshape[0][4] = -2.*lam0 * lam1;
        rshape[0][5] = -2.*lam1 * lam2;
        rshape[0][6] = -2.*lam2 * lam0;
        rshape[0][7] = -2.*lam0 * lam3;
        rshape[0][8] = -2.*lam1 * lam3;
        rshape[0][9] = -2.*lam2 * lam3;

        rshape[1][4] = 2.*(lam0 - lam1);
        rshape[1][5] = -2.*lam2;
        rshape[1][6] = 2.*lam2;
        rshape[1][7] = 2.*lam3;
        rshape[1][8] = -2.*lam3;
        rshape[1][9] = 0.;

        rshape[2][4] = 2.*lam1;
        rshape[2][5] = -2.*lam1;
        rshape[2][6] = 2.*(lam0 - lam2);
        rshape[2][7] = 2.*lam3;
        rshape[2][8] = 0.;
        rshape[2][9] = -2.*lam3;

        rshape[3][4] = 2.*lam1;
        rshape[3][5] = 0.;
        rshape[3][6] = 2.*lam2;
        rshape[3][7] = 2.*(lam0 - lam3);
        rshape[3][8] = -2.*lam1;
        rshape[3][9] = -2.*lam2;
      }
      break;
    }

    case Hexa:
    {
      const double oito = 1.0 / 8.0;
      static const double node[8][3] =
      {
        { -1.0, -1.0, -1.0},
        { 1.0, -1.0, -1.0},
        { 1.0,  1.0, -1.0},
        { -1.0,  1.0, -1.0},
        { -1.0, -1.0,  1.0},
        { 1.0, -1.0,  1.0},
        { 1.0,  1.0,  1.0},
        { -1.0,  1.0,  1.0}
      };

      // adjust to ordering given in carp manual
      static int v[8] = {4, 7, 6, 5, 0, 1, 2, 3};


      for (int i = 0; i < 8; i++) {
        // shape function
        rshape[0][i] = oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t. xi
        rshape[1][i] = node[v[i]][0] * oito * (1. + ip.y * node[v[i]][1]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t. eta
        rshape[2][i] = node[v[i]][1] * oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t zeta
        rshape[3][i] = node[v[i]][2] * oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]);
      }

      break;
    }

    default:
      fprintf(stderr, "%s: Unimplemented element type! Aborting!\n", __func__);
      exit(1);
  }
}

/**
* @brief Compute Jacobian matrix from the real element to the reference element.
*
* @param rshape  Reference element shape derivatives.
* @param npts    Number of points.
* @param pts     Points array.
* @param J       Jacobian matrix.
*/
inline void jacobian_matrix(const dmat<double> & rshape,
                        const int npts,
                        const Point* pts,
                        double *J)
{
  // zero the 3x3 jacobian matrix
  memset (J, 0, 9 * sizeof(double) );
  // note that ref_shape hold shape functions in [0][i] and derivatives in [k][i]
  // if element is 2 dimensional then [3][i] is set to 0!
  for (int i = 0; i < npts; i++)
  {
    J[0] += rshape[1][i] * pts[i].x;
    J[1] += rshape[1][i] * pts[i].y;
    J[2] += rshape[1][i] * pts[i].z;
    J[3] += rshape[2][i] * pts[i].x;
    J[4] += rshape[2][i] * pts[i].y;
    J[5] += rshape[2][i] * pts[i].z;
    J[6] += rshape[3][i] * pts[i].x;
    J[7] += rshape[3][i] * pts[i].y;
    J[8] += rshape[3][i] * pts[i].z;
  }
}

inline void invert_jacobian_matrix(const elem_t type, double* J, double & detJ)
{
  switch(type)
  {
    // all 3D elems
    default:
    case Tetra:
      invert_3x3(J, detJ);
      break;

    // 2D elems
    case Quad:
    case Tri: {
      double J2[4] = {J[0], J[1], J[3], J[4]};

      invert_2x2(J2, detJ);
      detJ = fabs(detJ);

      J[0] = J2[0]; J[1] = J2[1]; J[2] = 0.0;
      J[3] = J2[2]; J[4] = J2[3]; J[5] = 0.0;
      J[6] = 0.0;   J[7] = 0.0;   J[8] = 0.0;
      break;
    }

    // 1D elem
    case Line:
      detJ = J[0];
      J[0] = 1.0 / detJ;
      break;
  }
}

/**
* @brief Compute shape derivatives for an element, based
* on the shape derivatives of the associated reference element.
*
* @param iJ      Inverse Jacobian from the reference element to the real element.
* @param rshape  Reference shape funciton and derivatives.
* @param ndof    Number of d.o.f.
* @param shape   Shape derivatives.
*/
inline void shape_deriv(const double *iJ,
                    const dmat<double> & rshape,
                    const int ndof,
                    dmat<double> & shape)
{
  for (int in = 0; in < ndof; in++)
  {
    shape[1][in] = iJ[0] * rshape[1][in] +
                   iJ[1] * rshape[2][in] +
                   iJ[2] * rshape[3][in];

    shape[2][in] = iJ[3] * rshape[1][in] +
                   iJ[4] * rshape[2][in] +
                   iJ[5] * rshape[3][in];

    shape[3][in] = iJ[6] * rshape[1][in] +
                   iJ[7] * rshape[2][in] +
                   iJ[8] * rshape[3][in];
  }
}


/**
* @brief Comfort class. Provides getter functions to access the mesh member variables
* more comfortably.
*
* @tparam T  Integer type.
* @tparam S  Floating point type.
*/
template<class T, class S>
class element_view
{
  private:
  const meshdata<T, S> & _mesh;    ///< Mesh reference.
  const vector<T> & _glob_numbr;   ///< the global numbering
  T        _esize;                 ///< The number of connectivity indices of that element.
  const T* _offset_con;            ///< A pointer to the start of the element in the mesh connectivity.
  size_t   _eidx;                  ///< The element index of the view.
  int      _rank;                  ///< The rank of the process. Used to access layouts.

  public:
  /**
  * @brief Constructor. Initializes to element index 0.
  *
  * @param [in] mesh The mesh which should be linked to the element view.
  */
  element_view(const meshdata<T, S> & mesh, const SF_nbr nbr) :
               _mesh(mesh), _glob_numbr(_mesh.get_numbering(nbr))
  {
    this->set_elem(0);
    MPI_Comm_rank(_mesh.comm, &_rank);
  }

  /**
  * @brief Set the view to a new element.
  *
  * @param [in] eidx An element index.
  */
  inline void set_elem(size_t eidx)
  {
    _eidx = eidx;

    T offset    = _mesh.dsp[_eidx];
    _esize      = _mesh.dsp[_eidx+1] - offset;
    _offset_con = _mesh.con.data()   + offset;
  }

  /**
  * @brief Select next element if possible.
  *
  * @return Whether there was a next element.
  */
  inline bool next()
  {
    if(_eidx < (_mesh.l_numelem - 1) )
    {
      this->set_elem(_eidx + 1);
      return true;
    }
    else
      return false;
  }

  /**
  * @brief Getter function for the number of nodes.
  *
  * @return The number of nodes.
  */
  inline T num_nodes() const
  {
    return _esize;
  }

  /**
  * @brief Getter function for the element type.
  *
  * @return The element type.
  */
  inline elem_t type() const
  {
    return _mesh.type[_eidx];
  }

  /**
  * @brief Getter function for the element tag.
  *
  * @return The element tag.
  */
  inline T tag() const
  {
    return _mesh.tag[_eidx];
  }

  /**
  * @brief Access the connectivity information.
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Local index w.r.t. to the local domain. Use the mesh numberings to access global indices.
  */
  inline const T & node(short nidx) const
  {
    return _offset_con[nidx];
  }

  /**
  * @brief Access the connectivity information.
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Local index w.r.t. to the local domain. Use the mesh numberings to access global indices.
  */
  inline const T & global_node(short nidx) const
  {
    return _glob_numbr[_offset_con[nidx]];
  }


  /**
  * @brief Access the connectivity information.
  *
  * @return Local connectivity indices.
  */
  inline const T* nodes() const
  {
    return _offset_con;
  }
  /**
  * @brief Access vertex coordinates
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Vertex coordinate.
  */
  inline Point coord(short nidx) const
  {
    T idx = _offset_con[nidx];
    return {_mesh.xyz[idx*3+0], _mesh.xyz[idx*3+1], _mesh.xyz[idx*3+2]};
  }

  /**
  * @brief Get element fiber direction.
  *
  * @return Fiber direction.
  */
  inline Point fiber() const
  {
    return {_mesh.fib[_eidx*3+0], _mesh.fib[_eidx*3+1], _mesh.fib[_eidx*3+2]};
  }
  /**
  * @brief Get element sheet direction.
  *
  * @return Sheet direction.
  */
  inline Point sheet() const
  {
    if(_mesh.she.size())
      return {_mesh.she[_eidx*3+0], _mesh.she[_eidx*3+1], _mesh.she[_eidx*3+2]};
    else
      return {0,0,0};
  }

  /**
  * @brief Check if a sheet direction is present.
  *
  * @return Whether sheet direction is present.
  */
  bool has_sheet() const
  {
    return _mesh.she.size() > 0;
  }

  /**
  * @brief Get currently selected element index.
  *
  * @return Element index.
  */
  inline size_t element_index() const
  {
    return _eidx;
  }

  /**
  * @brief Get currently selected element index.
  *
  * @return Element index.
  */
  inline size_t global_element_index() const
  {
    return _mesh.epl.algebraic_layout()[_rank] + _eidx;
  }

  inline short num_dof(short order) const
  {
    return SF::num_dof(_mesh.type[_eidx], order);
  }

  inline void integration_points(const short order, Point* ip, double* w, int & nint) const
  {
    general_integration_points(_mesh.type[_eidx], order, ip, w, nint);
  }

  inline short dimension() const
  {
    switch(_mesh.type[_eidx])
    {
      default:
      case Tetra:
        return 3;

      case Tri:
      case Quad:
        return 2;

      case Line:
        return 1;
    }
  }
};

/**
* @brief Abstract matrix integration base class.
*
* @tparam T  Integer type used in associated element_view (thus in the mesh)
* @tparam S  Floating point type used in associated element_view (thus in the mesh)
*/
template<class T, class S>
class matrix_integrator
{
  protected:
  void zero_buff(double* buff, T nrows, T ncols)
  {
    for(T i=0; i<nrows; i++)
      for(T j=0; j<ncols; j++)
        buff[i*ncols+j] = 0.0;
  }

  public:
  /// compute the element matrix for a given element.
  virtual void operator() (const element_view<T,S> & elem, double* buff) = 0;
  /// return (by referrence) the row and column dimensions
  virtual void dpn(T & row_dpn, T & col_dpn) = 0;
};

/**
* @brief Abstract vector integration base class.
*
* @tparam T  Integer type used in associated element_view (thus in the mesh)
* @tparam S  Floating point type used in associated element_view (thus in the mesh)
*/
template<class T, class S>
class vector_integrator
{
  protected:
  void zero_buff(double* buff, T nrows)
  {
    for(T i=0; i<nrows; i++) buff[i] = 0.0;
  }

  public:
  /// compute the element matrix for a given element.
  virtual void operator() (const element_view<T,S> & elem, double* buff) = 0;
  /// return (by referrence) the row and column dimensions
  virtual void dpn(T & dpn) = 0;
};

/**
* @brief Compute the node-to-node connectivity.
*
* @param mesh     The mesh.
* @param n2n_cnt  Counts of the connectivity matrix rows.
* @param n2n_con  Column indices of the connectivity matrix.
*/
template<class T, class S>
inline void nodal_connectivity_graph(const meshdata<T, S> & mesh,
                              vector<T> & n2n_cnt,
                              vector<T> & n2n_con)
{
  vector<T> e2n_cnt, n2e_cnt, n2e_con;
  const vector<T> & e2n_con = mesh.con;

  cnt_from_dsp(mesh.dsp, e2n_cnt);

  transpose_connectivity(e2n_cnt, e2n_con, n2e_cnt, n2e_con);
  multiply_connectivities(n2e_cnt, n2e_con,
                          e2n_cnt, e2n_con,
                          n2n_cnt, n2n_con);
}

/**
* @brief Compute the maximum number of node-to-node edges for a mesh.
*
* @tparam T    Integer type.
* @tparam S    Floating point type.
* @param mesh  The mesh.
*
* @return The computed maximum.
*/
template<class T, class S>
int max_nodal_edgecount(const meshdata<T,S> & mesh)
{
  vector<T> n2n_cnt, n2n_con;
  nodal_connectivity_graph(mesh, n2n_cnt, n2n_con);

  int nmax = 0;
  for(size_t nidx=0; nidx < n2n_cnt.size(); nidx++)
    if(nmax < n2n_cnt[nidx]) nmax = n2n_cnt[nidx];

  MPI_Allreduce(MPI_IN_PLACE, &nmax, 1, MPI_INT, MPI_MAX, mesh.comm);

  return nmax;
}


/**
* @brief Compute canonical indices from nodal indices and dpn.
*
* @param nidx  Nodal indices in local indexing.
* @param nbr   Numbering to use.
* @param esize Element size.
* @param dpn   Degrees of freedom per node.
* @param cidx  Canonical indices.
*/
template<class T, class V>
inline void canonic_indices(const T* nidx,
                     const T* nbr,
                     const T esize,
                     const short dpn,
                     V* cidx)
{
  for(T i=0; i<esize; i++)
    for(short j=0; j<dpn; j++)
      cidx[i*dpn + j] = nbr[nidx[i]]*dpn + j;
}

// forward declaration
template<class T, class S> class petsc_vector;

/**
* @brief FEM matrix class.
*
* It connects a mesh with a PETSc matrix.
*
* @tparam T  Integer type.
* @tparam S  Floating point type.
*/
template<class T, class S>
class petsc_matrix
{
  public:

  int  NRows;
  int  NCols;
  int  row_dpn;
  int  col_dpn;
  int  lsize;         //!< size of local matrix (\#locally stored rows)
  int  start;         //!< start row index of local matrix portion
  int  stop;          //!< stio row index of local matrix portion
  mutable Mat  data;

  const meshdata<T, S> * mesh;

  // empty constructor
  petsc_matrix() :
  NRows(0), NCols(0),
  row_dpn(0), col_dpn(0),
  lsize(0), start(0), stop(0),
  data(NULL),
  mesh(NULL)
  {}

  // destructor
  ~petsc_matrix()
  {
    if(data) MatDestroy(&data);
  }

  /// algebraic init
  inline void init(PetscInt iNRows, PetscInt iNCols,
            PetscInt ilrows, PetscInt ilcols,
            PetscInt loc_offset,
            PetscInt mxent)
  {
    //InitializeMatrixAlgebraic(&data, M, N, m, n, loc_offset, dpn, mxent, mstruct, false, false);
    NRows = iNRows;
    NCols = iNCols;
    lsize = ilrows;
    start = loc_offset;
    stop  = start+lsize;
    //CreatePetscMatrix(&data, NRows, NCols, ilrows, ilcols, mxent, mxent, false);
    MatCreateAIJ(PETSC_COMM_WORLD, ilrows, ilcols, NRows, NCols, mxent, PETSC_NULL, mxent, PETSC_NULL, &data);
    MatSetFromOptions(data);
    MatMPIAIJSetPreallocation(data, mxent, PETSC_NULL, mxent, PETSC_NULL);
    //MatGetOwnershipRange(data, &mat->start, &mat->stop);
    MatSetOption(data, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
  }

  /// init from mesh
  inline void init(const meshdata<T, S> & imesh,
                   const PetscInt irow_dpn,
                   const PetscInt icol_dpn,
                   const PetscInt max_edges = -1)
  {
    mesh = &imesh;
    row_dpn = irow_dpn;
    col_dpn = icol_dpn;

    int rank;
    MPI_Comm_rank(mesh->comm, &rank);

    PetscInt M = mesh->pl.num_global_idx()*row_dpn;
    PetscInt N = mesh->pl.num_global_idx()*col_dpn;
    PetscInt m = mesh->pl.num_algebraic_idx()*row_dpn;
    PetscInt n = mesh->pl.num_algebraic_idx()*col_dpn;

    PetscInt nmax = max_edges;
    if(max_edges < 0) nmax = max_nodal_edgecount(*mesh);

    PetscInt loc_offset = mesh->pl.algebraic_layout()[rank];

    init(M, N, m, n, loc_offset, nmax*col_dpn);
    zero();
  }

  inline void zero()
  {
    MatZeroEntries(data);
  }

  inline void mult(Vec x, Vec b) const
  {
    MatMult(data, x, b);
  }
  inline void mult(petsc_vector<T,S> & x, petsc_vector<T,S> & b) const
  {
    this->mult(x.data, b.data);
  }

  inline void mult_LR(Vec L, Vec R)
  {
    MatDiagonalScale(data, L, R);
  }

  inline void add_scaled_matrix(const petsc_matrix<T,S> & mat, const PetscReal s,
                                 const bool same_nnz)
  {
    MatStructure NNZ = same_nnz ? SAME_NONZERO_PATTERN : DIFFERENT_NONZERO_PATTERN;
    MatAXPY(data, s, mat.data, NNZ);
  }

  inline void diag_add(Vec diag)
  {
    MatDiagonalSet(data, diag, ADD_VALUES);
  }
  inline void diag_add(petsc_vector<T,S> & diag)
  {
    MatDiagonalSet(data, diag.data, ADD_VALUES);
  }

  inline void finish_assembly()
  {
    MatAssemblyBegin(data, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (data, MAT_FINAL_ASSEMBLY);
  }

  inline void scale(PetscReal s)
  {
    MatScale(data, s);
  }

  inline void operator= (const petsc_matrix<T,S> & inp_mat)
  {
    NRows   = inp_mat.NRows;
    NCols   = inp_mat.NCols;
    row_dpn = inp_mat.row_dpn;
    col_dpn = inp_mat.col_dpn;
    lsize   = inp_mat.lsize;
    start   = inp_mat.start;
    stop    = inp_mat.stop;
    mesh    = inp_mat.mesh;

    if(data) MatDestroy(&data);
    MatDuplicate(inp_mat.data, MAT_COPY_VALUES, &data);
  }

  inline void write(const char* filename)
  {
    PetscViewer viewer;
    PetscViewerBinaryOpen(mesh->comm, filename, FILE_MODE_WRITE, &viewer);
    MatView(data, viewer);
    PetscViewerDestroy(&viewer);
  }
};

/**
* @brief A class encapsulating a petsc vector.
*
* It is connected to a mesh and offers some convenience features
* w.r.t. setup and function calls.
*
*/
template<class T, class S>
class petsc_vector {

  public:

  enum ltype {algebraic, nodal, unset};

  Vec                   data   = NULL;    ///< the PETSc vector pointer
  const meshdata<T,S> * mesh   = NULL;    ///< the connected mesh
  int                   dpn    = 0;       ///< d.o.f. per mesh vertex.
  ltype                 layout = unset;   ///< used vector layout (nodal, algebraic, unset)

  petsc_vector()
  {}

  ~petsc_vector()
  {
    if(data) VecDestroy(&data);
  }

  petsc_vector(const meshdata<T,S> & imesh, int idpn, ltype inp_layout) {
    init(imesh, idpn, inp_layout);
  }

  petsc_vector(int igsize, int ilsize, int idpn, ltype ilayout) {
    init(igsize, ilsize, idpn, ilayout);
  }

  petsc_vector(const petsc_vector<T,S> & vec) {
    init(vec);
  }

  /**
  * @brief Init the vector dimensions based on a give mesh.
  *
  * @param imesh  The mesh defining the parallel layout of the vector.
  * @param idpn   The number of d.o.f. per mesh vertex.
  */
  inline void init(const meshdata<T,S> & imesh, int idpn, ltype inp_layout)
  {
    mesh = &imesh;
    dpn = idpn;
    int N = 0, n = 0;

    switch(inp_layout) {
      case algebraic:
        N = mesh->pl.num_global_idx()*dpn;
        n = mesh->pl.num_algebraic_idx()*dpn;
        layout = algebraic;
        break;
      case nodal:
        N = mesh->l_numpts*dpn;
        MPI_Allreduce(MPI_IN_PLACE, &N, 1, MPI_INT, MPI_SUM, mesh->comm);
        n = mesh->l_numpts*dpn;
        layout = nodal;
        break;
      default: break;
    }

    if(data) VecDestroy(&data);
    VecCreateMPI(PETSC_COMM_WORLD, n, N, &data);
  }

  /**
  * @brief Init parallel petsc vector directly with sizes.
  *
  * @param igsize  Global size
  * @param ilsize  Local size
  * @param idpn    Number of d.o.f. used per mesh node
  * @param ilayout Vector layout w.r.t. used mesh.
  */
  inline void init(int igsize, int ilsize, int idpn = 1, ltype ilayout = unset) {
    if(data) VecDestroy(&data);

    int ierr;
    if (ilsize == igsize && igsize >= 0) {
      ierr = VecCreateSeq(PETSC_COMM_SELF, igsize, &data);
    }
    else if (ilsize >= 0) {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, ilsize, abs(igsize), &data);
    }
    else {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, abs(igsize), &data);
    }
    // ierr = VecSetFromOptions (v);
    // ierr = VecSetBlockSize(v, bs);

    mesh = NULL;
    dpn = idpn;
    layout = ilayout;
    set(0.0);
  }

  /**
  * @brief Initialize a vector from the setup of a given vector.
  *
  * @param vec Vector to replicate the setup from.
  */
  inline void init(const petsc_vector<T,S> & vec) {
    if(data) VecDestroy(&data);

    VecDuplicate(vec.data, &data);

    mesh   = vec.mesh;
    dpn    = vec.dpn;
    layout = vec.layout;
    set(0.0);
  }

  /**
  * @brief Set vector values.
  *
  * @param idx  The global indices where to set.
  * @param vals The values to set.
  */
  inline void set(const vector<PetscInt> & idx, const vector<PetscReal> & vals, const bool additive = false)
  {
    InsertMode mode = additive ? ADD_VALUES : INSERT_VALUES;
    VecSetValues(data, idx.size(), idx.data(), vals.data(), mode);
    VecAssemblyBegin(data);
    VecAssemblyEnd(data);
  }
  /**
  * @brief Set vector indices to one value.
  *
  * @param idx  The indices where to set.
  * @param val  The value to set.
  */
  inline void set(const vector<PetscInt> & idx, const PetscReal val)
  {
    vector<PetscReal> vals(idx.size(), val);
    set(idx, vals);
  }
  /// set whole vector to one val
  inline void set(const PetscReal val)
  {
    VecSet(data, val);
  }

  /// scale vector
  inline void operator *= (const PetscReal sca)
  {
    // VecScale(data, sca);
    PetscReal* w = this->ptr();
    for(PetscInt i=0; i<this->lsize(); i++) w[i] *= sca;
    this->release_ptr(w);
  }
  /// scale vector
  inline void operator /= (const PetscReal sca)
  {
    (*this) *= 1.0 / sca;
  }

  inline void operator *= (petsc_vector<T,S> & vec)
  {
    PetscInt mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr();
    PetscReal* v = vec.ptr();

    for(PetscInt i=0; i < mysize; i++) w[i] *= v[i];

    this->release_ptr(w);
    this->release_ptr(v);
  }

  void add_scaled(petsc_vector<T,S> & vec, PetscReal k)
  {
    PetscInt mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr(), *r = vec.ptr();

    for(PetscInt i=0; i<mysize; i++)
      w[i] += r[i] * k;

    this->release_ptr(w);
    vec.release_ptr(r);
  }

  /// vector add
  inline void operator += (petsc_vector<T,S> & vec)
  {
    this->add_scaled(vec, 1.0);
  }
  /// vector sub
  inline void operator -= (petsc_vector<T,S> & vec)
  {
    this->add_scaled(vec, -1.0);
  }

  /// scalar add, equivalent to VecShift
  inline void operator += (PetscReal c)
  {
    PetscReal* w = this->ptr();

    for(PetscInt i=0; i<this->lsize(); i++) w[i] += c;

    this->release_ptr(w);
  }

  /// deep copy vector
  inline void operator= (const vector<PetscReal> & rhs)
  {
    assert(this->lsize() == rhs.size());

    PetscReal *w = this->ptr();
    const PetscReal *r = rhs.data();

    for(size_t i=0; i<rhs.size(); i++)
      w[i] = r[i];

    this->release_ptr(w);
  }
  /// deep copy petsc vector
  inline void operator= (petsc_vector<T,S> & rhs)
  {
    int mylsize = this->lsize();

    assert(mylsize == rhs.lsize());
    PetscReal* w = this->ptr(), *r = rhs.ptr();

    for(PetscInt i=0; i<mylsize; i++) w[i] = r[i];

    this->release_ptr(w);
    rhs.release_ptr(r);
  }

  inline void shallow_copy(petsc_vector<T,S> & v)
  {
    data   = v.data;
    mesh   = v.mesh;
    dpn    = v.dpn;
    layout = v.layout;
  }

  /// get local size
  inline PetscInt lsize() const
  {
    int loc_size; VecGetLocalSize(data, &loc_size);
    return loc_size;
  }

  /// get global size
  inline PetscInt gsize() const
  {
    int glb_size; VecGetSize(data, &glb_size);
    return glb_size;
  }

  inline void get_ownership_range(PetscInt & start, PetscInt & stop)
  {
    VecGetOwnershipRange(data, &start, &stop);
  }

  /// get pointer to local data
  inline PetscReal* ptr()
  {
    PetscReal* p;
    // get pointer to local data
    VecGetArray(data, &p);
    return p;
  }

  /// release pointer to local data
  inline void release_ptr(PetscReal* & p)
  {
    // release local data
    VecRestoreArray(data, &p);
  }
  /// L2 norm
  inline PetscReal mag()
  {
    PetscReal ret;
    VecNorm(data, NORM_2, &ret);
    return ret;
  }
  inline PetscReal sum()
  {
    PetscReal ret;
    VecSum(data, &ret);
    return ret;
  }

  /// return whether the vector is initialized
  bool is_init()
  {
    return this->data != NULL;
  }

  /**
  * @brief Write a vector to HD in ascii
  *
  * @param file           The file name.
  * @param write_header   Whether to write the vector size as a header.
  */
  inline size_t write_ascii(const char* file, bool write_header)
  {
    int size, rank;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    int glb_size = this->gsize();
    int loc_size = this->lsize();

    int err = 0;
    FILE* fd = NULL;
    long int nwr = 0;

    if(rank == 0) {
      fd = fopen(file, "w");
      if(!fd) err = 1;
    }

    MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_INT, MPI_MAX, comm);
    if(err) {
      treat_file_open_error(file, __func__, errno, false, comm);
      return nwr;
    }

    PetscReal* p = this->ptr();

    if(rank == 0) {
      if(write_header)
        fprintf(fd, "%d\n", glb_size);

      for(int i=0; i<loc_size/dpn; i++) {
        for(int j=0; j<dpn; j++)
          nwr += fprintf(fd, "%f ", p[i*dpn+j]);
        nwr += fprintf(fd, "\n");
      }

      vector<PetscReal> wbuff;
      for(int pid=1; pid < size; pid++)
      {
        int rsize;
        MPI_Status stat;

        MPI_Recv(&rsize, 1, MPI_INT, pid, SF_MPITAG, comm, &stat);
        wbuff.resize(rsize);
        MPI_Recv(wbuff.data(), rsize*sizeof(PetscReal), MPI_BYTE, pid, SF_MPITAG, comm, &stat);

        for(int i=0; i<rsize/dpn; i++) {
          for(int j=0; j<dpn; j++)
            nwr += fprintf(fd, "%f ", wbuff[i*dpn+j]);
          nwr += fprintf(fd, "\n");
        }
      }
      fclose(fd);
    }
    else {
      MPI_Send(&loc_size, 1, MPI_INT, 0, SF_MPITAG, comm);
      MPI_Send(p, loc_size*sizeof(PetscReal), MPI_BYTE, 0, SF_MPITAG, comm);
    }

    this->release_ptr(p);
    MPI_Bcast(&nwr, 1, MPI_LONG, 0, comm);

    return nwr;
  }

  /**
  * @brief Write a vector to HD in binary. File descriptor is already set up.
  *
  * File descriptor is not closed by this function.
  *
  * @param fd   The already set up file descriptor.
  */
  template<typename V>
  inline size_t write_binary(FILE* fd)
  {
    int size, rank;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &size);

    long int loc_size = this->lsize();
    PetscReal* p = this->ptr();

    vector<V> buff(loc_size);
    for(long int i=0; i<loc_size; i++) buff[i] = p[i];

    long int nwr = root_write(fd, buff, comm);

    this->release_ptr(p);
    return nwr;
  }

  /// write binary. Open file descriptor myself.
  template<typename V>
  inline size_t write_binary(std::string file)
  {
    size_t nwr = 0;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    int rank; MPI_Comm_rank(comm, &rank);

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "w");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nwr = this->write_binary<V>(fd);
      fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, mesh->comm);
    }

    return nwr;
  }

  template<typename V>
  inline size_t read_binary(FILE* fd)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;

    size_t loc_size = this->lsize();
    PetscReal* p = this->ptr();
    vector<V> buff(loc_size);

    size_t nrd = root_read(fd, buff, comm);

    for(size_t i=0; i<loc_size; i++)
      p[i] = buff[i];

    this->release_ptr(p);

    return nrd;
  }

  template<typename V>
  inline size_t read_binary(std::string file)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;

    size_t nrd = 0;
    int rank; MPI_Comm_rank(comm, &rank);

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "r");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nrd = read_binary<V>(fd);
      fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, mesh->comm);
    }

    return nrd;
  }

  inline size_t read_ascii(FILE* fd)
  {
    size_t loc_size = this->lsize();
    PetscReal* p = this->ptr();

    size_t nrd = root_read_ascii(fd, p, loc_size, mesh->comm, false);
    return nrd;
  }

  inline size_t read_ascii(std::string file)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    int rank; MPI_Comm_rank(comm, &rank);

    size_t nrd = 0;

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "r");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nrd = read_ascii(fd);
      if(fd) fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, mesh->comm);
    }

    return nrd;
  }
};



/**
* @brief Generalized matrix assembly.
*
* All the assembly details -- both the computation as well as the parameters handling --
* are encapsulated in the used integrator.
*
* @param mat         matrix to add values into
* @param domain      The domain we compute the integral on
* @param integrator  The integral computation functor
*/
template<class T, class S>
inline void assemble_matrix(petsc_matrix<T,S> & mat,
                            meshdata<T,S> & domain,
                            matrix_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(mat.mesh->comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T row_dpn, col_dpn;
  integrator.dpn(row_dpn, col_dpn);
  assert(mat.row_dpn == row_dpn && mat.col_dpn == col_dpn);

  // allocate row / col index buffers
  vector<PetscInt> row_idx(SF_MAX_ELEM_NODES * row_dpn), col_idx(SF_MAX_ELEM_NODES * col_dpn);

  // allocate elem index buffer
  vector<PetscReal> ebuff(SF_MAX_ELEM_NODES * SF_MAX_ELEM_NODES * row_dpn * col_dpn);
  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, row_dpn, row_idx.data());
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, col_dpn, col_idx.data());

    // call integrator
    integrator(view, ebuff.data());

    // add values into system matrix
    MatSetValues(mat.data, nnodes*row_dpn, row_idx.data(),
                 nnodes*col_dpn, col_idx.data(), ebuff.data(), ADD_VALUES);
  }
  // finish assembly and progress output
  mat.finish_assembly();
}

template<class T, class S>
inline void assemble_lumped_matrix(petsc_matrix<T,S> & mat,
                                   meshdata<T,S> & domain,
                                   matrix_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(mat.mesh->comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T row_dpn, col_dpn;
  integrator.dpn(row_dpn, col_dpn);

  assert(mat.row_dpn == 1 && mat.row_dpn == mat.col_dpn);
  assert(mat.row_dpn == row_dpn);

  // allocate row / col index buffers
  vector<PetscInt> row_idx(SF_MAX_ELEM_NODES * row_dpn);

  // allocate elem index buffer
  vector<PetscReal> ebuff(SF_MAX_ELEM_NODES * SF_MAX_ELEM_NODES * row_dpn * col_dpn);
  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, row_dpn, row_idx.data());

    // call integrator
    integrator(view, ebuff.data());

    // do lumping
    for(int i=0; i<nnodes; i++) {
      PetscReal rowsum = 0.0;

      for(int j=0; j<nnodes; j++)
          rowsum += ebuff[(i*nnodes) + j];

      // add values into system matrix
      MatSetValue(mat.data, row_idx[i], row_idx[i], rowsum, ADD_VALUES);
    }
  }

  // finish assembly and progress output
  mat.finish_assembly();
}

/**
* @brief Generalized vector assembly.
*
* All the assembly details -- both the computation as well as the parameters handling --
* are encapsulated in the used integrator.
*
* @param vec         vector to add values into
* @param domain      The domain we compute the integral on
* @param integrator  The integral computation functor
* @param prg         Progress display class
*/
template<class T, class S>
inline void assemble_vector(petsc_vector<T,S> & vec,
                     meshdata<T,S> & domain,
                     vector_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(vec.mesh->comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T dpn;
  integrator.dpn(dpn);
  assert(vec.dpn == dpn);

  // allocate row / col index buffers
  vector<PetscInt> idx(SF_MAX_ELEM_NODES * dpn);

  // allocate elem index buffer
  vector<PetscReal> ebuff(SF_MAX_ELEM_NODES * dpn);
  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, dpn, idx.data());

    // call integrator
    integrator(view, ebuff.data());

    // add values into system matrix
    VecSetValues(vec.data, nnodes*dpn, idx.data(), ebuff.data(), ADD_VALUES);
  }
  // finish assembly and progress output
  VecAssemblyBegin(vec.data);
  VecAssemblyEnd(vec.data);
}


/*
 * A basic structure to hold the most common datastructs that one might need for a PETSC
 * solver. The initialization of solvers varies hugely, as such we do not present an
 * initialization workflow on this code level. We rather expect the user to set up the
 * linear solvers in the context of the PDE type that is adressed. -Aurel
 *
 */
template<class T, class S>
struct petsc_solver {
  enum norm_t {absPreResidual, absUnpreResidual, relResidual, absPreRelResidual, norm_unset};

  petsc_matrix<T,S> * matrix = NULL;
  const char*         name         = "unnamed";
  const char*         options_file = NULL;

  norm_t              norm   = norm_unset;
  int                 max_it = -1;

  // solver statistics
  double       final_residual    = -1.0;                 //!< Holds the residual after convergence
  int          niter             = -1;                   //!< number of iterations

  KSPConvergedReason reason      = static_cast<KSPConvergedReason>(0);
  KSP          ksp       = NULL;
  MatNullSpace nullspace = NULL;

  void operator() (petsc_vector<T,S> & x, petsc_vector<T,S> & b)
  {
    assert (x.data != b.data);
    assert (ksp != NULL);

    const double solve_zero = 1e-16;
    const double NORMB = b.mag();

    if(NORMB > solve_zero)
    {
      KSPSolve(ksp, b.data, x.data);
      KSPGetIterationNumber(ksp, &niter);
      KSPGetConvergedReason(ksp, &reason);

      KSPGetResidualNorm(ksp, &final_residual);
    }
    else {
      niter = 0;
      final_residual = NORMB;
    }
  }
};

template<class T, class S>
void extract_element_data(const element_view<T,S> & view, petsc_vector<T,S> & vec, PetscReal* buffer)
{
  // the dpn has to be set for this to work
  int dpn = vec.dpn;
  typename petsc_vector<T,S>::ltype nodaltype = petsc_vector<T,S>::nodal;
  assert(dpn > 0);
  assert(vec.layout == nodaltype);

  PetscReal* pvec = vec.ptr();

  for(int i=0; i<view.num_nodes(); i++)
    for(int j=0; j<dpn; j++) {
      int idx = view.node(i)*dpn+j;
      buffer[i*dpn+j] = pvec[idx];
    }

  vec.release_ptr(pvec);
}

template<class T, class S>
void set_element_data(const element_view<T,S> & view, PetscReal* buffer, petsc_vector<T,S> & vec)
{
  // the dpn has to be set for this to work
  int dpn = vec.dpn;
  typename petsc_vector<T,S>::ltype nodaltype = petsc_vector<T,S>::nodal;
  assert(dpn > 0);
  assert(vec.layout == nodaltype);

  PetscReal* pvec = vec.ptr();

  for(int i=0; i<view.num_nodes(); i++)
    for(int j=0; j<dpn; j++) {
      int idx = view.node(i)*dpn+j;
      pvec[idx] = buffer[i*dpn+j];
    }

  vec.release_ptr(pvec);
}

}

#endif

