/**
* @file numbering.h
* @brief Classes related to mesh node renumbering.
* @author Aurel Neic
* @version
* @date 2017-02-14
*/


#ifndef _SF_NUMBERING_H
#define _SF_NUMBERING_H

namespace SF {

/**
* @brief    The abstract numbering class.
*
* @tparam T   Integer type.
* @tparam S   Floating point type.
*/
template<class T, class S>
class numbering {
public:

  /**
  * @brief  Add a numbering to a mesh.
  *
  * @param mesh  The mesh.
  */
  virtual void operator() (meshdata<T, S> & mesh) = 0;
};


/**
* @brief Functor class appyling a submesh renumbering
*
* @tparam T   Integer type.
* @tparam S   Floating point type.
*/
template<class T, class S>
class submesh_numbering : public numbering<T, S>
{
public:
  /**
  * @brief Renumber the global indexing of a submesh with respect to the globally ascending
  *        indexing of the reference mesh.
  *
  * @param submesh  [in,out]  The submesh.
  */
  inline void operator() (meshdata<T, S> & submesh)
  {
    MPI_Comm comm = submesh.comm;

    int size, rank;
    MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

    // determine global min and max indices
    vector<T> & rnod = submesh.get_numbering(NBR_REF);
    T gmax = global_max(rnod, comm);
    T gmin = global_min(rnod, comm);

    // block size
    T bsize = (gmax - gmin) / size + 1;

    // distribute indices uniquely and linearly ascending (w.r.t. rank index) --------------------
    vector<T> dest(submesh.l_numpts), sbuff(rnod);
    for(size_t i=0; i<dest.size(); i++)
      dest[i] = (rnod[i] - gmin) / bsize;
    binary_sort_copy(dest, sbuff);

    commgraph<size_t> grph;
    grph.configure(dest, comm);

    size_t rsize = sum(grph.rcnt);
    vector<T> rbuff(rsize);
    MPI_Exchange(grph, sbuff, rbuff, comm);

    // we need a data structure to store where we got the indices from
    vector<T> proc(rsize), acc_cnt(rsize, T(1));
    grph.source_ranks(proc);

    // compute local node set
    binary_sort_copy(rbuff, proc);
    unique_accumulate(rbuff, acc_cnt);
    rsize = rbuff.size();

    // communicate local sizes
    MPI_Allgather(&rsize, sizeof(size_t), MPI_BYTE, grph.rcnt.data(), sizeof(size_t), MPI_BYTE, comm);
    dsp_from_cnt(grph.rcnt, grph.rdsp);

    // now the new numbering can be derived from the data layout
    vector<T> new_nbr(rsize);
    interval(new_nbr, grph.rdsp[rank], grph.rdsp[rank+1]);

    // we send the old and new numberings back -------------------------------------------------
    sbuff.resize(proc.size());

    // expand the old numbering into the send buffer
    for(size_t i=0, idx=0; i<acc_cnt.size(); i++)
      for(T j=0; j<acc_cnt[i]; j++, idx++) sbuff[idx] = rbuff[i];
    dest.assign(proc.begin(), proc.end());
    binary_sort_copy(dest, sbuff); // permute for sending
    grph.configure(dest, comm);    // reconfigure comm graph
    rbuff.resize(sum(grph.rcnt));  // resize receive buffer
    MPI_Exchange(grph, sbuff, rbuff, comm);  // communicate

    vector<T> old_idx(rbuff);

    // expand the new numbering into the send buffer
    for(size_t i=0, idx=0; i<acc_cnt.size(); i++)
      for(T j=0; j<acc_cnt[i]; j++, idx++) sbuff[idx] = new_nbr[i];
    dest.assign(proc.begin(), proc.end());
    binary_sort_copy(dest, sbuff); // permute for sending
    MPI_Exchange(grph, sbuff, rbuff, comm);  // communicate

    vector<T> new_idx(rbuff);

    // add a new numbering to submesh -----------------------------------------
    // create mapping between original and new numbering
    index_mapping<T> map(old_idx, new_idx);
    vector<T> & snod = submesh.register_numbering(NBR_SUBMESH);
    snod.resize(rnod.size());
    // use mapping for new numbering
    for(size_t i=0; i < rnod.size(); i++) snod[i] = map.forward_map(rnod[i]);
  }
};


/**
* @brief Functor class generating a numbering optimized for PETSc.
*
* @tparam T   Integer type.
* @tparam S   Floating point type.
*/
template<class T, class S>
class petsc_numbering : public numbering<T, S>
{
private:
  const overlapping_layout<T> & _pl;
public:
  petsc_numbering(const overlapping_layout<T> & pl) : _pl(pl)
  {}

  /**
   *  @brief Generate a numbering as necessary for good PETSc parallel performance.
   *
   *  PETSc implicitely the following parallel layout of unknowns:
   *
   *  Rank  |  unkowns intervall
   *  ----- | -------------------
   *  0     |  [0,1,...,N0-1]
   *  1     |  [N0,N0+1,...,N1-1]
   *  ...   |  ...
   *
   *  For good parallel performance, the node indices used in the domain decomposition
   *  should match as closely as possible to the described layout. This makes a renumbering
   *  necessary.
   *
   *  @param [in,out]  mesh   The mesh.
   *
   *  @post A petsc numbering has been added to the mesh.
   *
   */
  inline void operator() (meshdata<T, S> & mesh)
  {
    vector<T> & petsc_idx = mesh.register_numbering(NBR_PETSC);
    petsc_idx.assign(_pl.num_local_idx(), -1);

    int size, rank;
    MPI_Comm comm = mesh.comm;
    MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);


    const vector<T> & alg_dsp = _pl.algebraic_layout();
    const vector<T> & alg_nod = _pl.algebraic_nodes();
    const T alg_start = alg_dsp[rank], alg_end = alg_dsp[rank+1];

    #if 1
    for(size_t i=0; i<alg_nod.size(); i++)
      petsc_idx[alg_nod[i]] = alg_start + i;
    #else
    vector<bool> is_alg(mesh.l_numpts, false);
    for(const T & n : alg_nod) is_alg[n] = true;

    size_t loc_petsc_idx = 0;
    for(size_t eidx = 0; eidx < mesh.l_numelem; eidx++) {
      for(T j = mesh.dsp[eidx]; j < mesh.dsp[eidx+1]; j++) {
        const T c = mesh.con[j];
        if(is_alg[c] && petsc_idx[c] == -1) {
          petsc_idx[c] = alg_start + loc_petsc_idx;
          loc_petsc_idx++;
        }
      }
    }

    assert(loc_petsc_idx == alg_nod.size());
    #endif
    // communicate the new numbering so that all nodes in the local DD domain
    // -- also the ones not in the algebraic range -- have a new index

    _pl.reduce(petsc_idx, "max");

    bool print_indexing = false;
    if(print_indexing) {
      for(int pid = 0; pid < size; pid++) {
        if(pid == rank) {
          for(size_t eidx = 0; eidx < 100; eidx++) {
            printf("rank %d elem %d: ", rank, int(eidx));
            for(T j = mesh.dsp[eidx]; j < mesh.dsp[eidx+1] - 1; j++) {
              const T c = mesh.con[j];
              printf("%d ", petsc_idx[c]);
            }
            printf("%d\n", petsc_idx[mesh.con[mesh.dsp[eidx+1] - 1]]);
          }
        }
        MPI_Barrier(PETSC_COMM_WORLD);
      }
    }
  }
};

}

#endif
