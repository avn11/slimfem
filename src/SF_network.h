/**
* @file SF_network.h
* @brief Functions related to network communication.
* @author Aurel Neic
* @version
* @date 2017-02-14
*/

#ifndef _SF_NETWORK_H
#define _SF_NETWORK_H

namespace SF {

/**
 * \brief Exchange data in parallel over MPI.
 *
 * \param [in]  grph  The communications graph.
 * \param [in]  send  The send buffer.
 * \param [out] revc  The receive buffer.
 *
 */
template<class T, class S>
inline void MPI_Exchange(commgraph<T> & grph,
                         vector<S> & send,
                         vector<S> & recv,
                         MPI_Comm comm)
{
  int size, rank;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  size_t numsend = 0, numrecv = 0;
  for(int i=0; i<size; i++) {
    if(grph.scnt[i]) numsend++;
    if(grph.rcnt[i]) numrecv++;
  }

  vector<MPI_Request> sreq(numsend), rreq(numrecv);
  vector<MPI_Status> stat(numsend > numrecv ? numsend : numrecv);

  size_t sidx = 0, ridx = 0;
  for(int i=0; i<size; i++) {
    if(grph.rcnt[i])
      MPI_Irecv(recv.data() + grph.rdsp[i], grph.rcnt[i]*sizeof(S), MPI_BYTE, i, SF_MPITAG, comm, rreq.data()+ridx++);
    if(grph.scnt[i])
      MPI_Isend(send.data() + grph.sdsp[i], grph.scnt[i]*sizeof(S), MPI_BYTE, i, SF_MPITAG, comm, sreq.data()+sidx++);
  }

  if( (ridx != numrecv) || (sidx != numsend) )
    fprintf(stderr, "Rank %d: MPI_Exchange error! \n", rank);

  MPI_Waitall(ridx, rreq.data(), stat.data());
  MPI_Waitall(sidx, sreq.data(), stat.data());
}

/**
* @brief Compute the global minimum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global minimum.
*/
template<class T>
inline T global_min(const vector<T> & vec, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);

  vector<T> mins(size);

  T lmin = *std::min_element(vec.begin(), vec.end());
  MPI_Allgather(&lmin, sizeof(T), MPI_BYTE, mins.data(), sizeof(T), MPI_BYTE, comm);
  T gmin = *std::min_element(mins.begin(), mins.end());

  return gmin;
}
/**
* @brief Compute the global maximum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global maximum.
*/
template<class T>
inline T global_max(const vector<T> & vec, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);

  vector<T> max(size);

  T lmax = *std::max_element(vec.begin(), vec.end());
  MPI_Allgather(&lmax, sizeof(T), MPI_BYTE, max.data(), sizeof(T), MPI_BYTE, comm);
  T gmax = *std::max_element(max.begin(), max.end());

  return gmax;
}
/**
* @brief Compute the global maximum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global maximum.
*/
template<class T>
inline T global_max(const T val, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);
  vector<T> max(size);

  MPI_Allgather(&val, sizeof(T), MPI_BYTE, max.data(), sizeof(T), MPI_BYTE, comm);
  T gmax = *std::max_element(max.begin(), max.end());

  return gmax;
}

}
#endif
