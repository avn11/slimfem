/**
* @file progress.hpp
* @brief CLI progress class.
* @author Aurel Neic
* @version 
* @date 2017-02-11
*/

#ifndef _MT_PROGRESS_H
#define _MT_PROGRESS_H

/**
* @brief Base class for tracking progress.
*
* @tparam T Integer type
*/
template<class T>
class progress
{
  protected:
  T _num_next;        ///< number of times next will be triggered
  std::string _msg;   ///< message that will be displayed before progress information.

  public:
  progress(T num_next, const char* msg) :
              _num_next(num_next),
              _msg(msg)
  {}
  virtual ~progress()
  {}

  virtual void next() = 0;
  virtual void increment(T inc) = 0;
  virtual void finish() = 0;
};

/**
* @brief Display progress as a percentage
*
* @tparam T Integer type.
*/
template<class T>
class progress_percent : public progress<T>
{
  private:
  T _counter;          ///< progress counter
  T _threshold;        ///< counter threshold for displaying
  std::string _blank;  ///< blank space before the progress is displayed

  /**
  * @brief Display the progress message
  *
  * @param percentage The displayed percentage
  */
  void display(T percentage)
  {
    const std::string & msg = progress<T>::_msg;
    
    printf("\r%s%s%d %c", msg.c_str(), _blank.c_str(), int(percentage), '%');
    fflush(stdout);
  }

  /**
  * @brief Evaluate if a new display is needed
  */
  void evaluate()
  {
    const T & num_next = progress<T>::_num_next;
    T prog = T(float(_counter) / float(num_next) * 100.0f);
    
    if(prog > _threshold)
    {
      _threshold = prog;
      this->display(prog);
    }
  }

  public:
  progress_percent(T num_next, const char* msg, T start = 60) : 
                      progress<T>(num_next, msg),
                      _counter(0),
                      _threshold(0)
  {
    int size = start - progress<T>::_msg.size();
    if(size < 0) size = 0;

    _blank.assign(size, ' ');
    this->display(0);
  }

  /// increment progress counter by one
  void next()
  {
    _counter++;
    this->evaluate();
  }

  /// increment progress counter by a custom value
  void increment(T inc)
  {
    _counter += inc;
    this->evaluate();
  }

  /// finish up progress display
  void finish()
  {
    this->display(100);
    printf("\n");
  }
};


/**
* @brief Display progress as a bar
*
* @tparam T Integer type
*/
template<class T>
class progress_bar : public progress<T>
{
  private:
  T _counter;     ///< progress counter
  T _threshold;   ///< threshold for displaying
  
  const T _bar_len;       ///< total amount of characters representing progress
  const char _bar_char;   ///< character used to fill the bar

  std::string _bar;       ///< bar
  std::string _blank_a;   ///< blank space before bar starts
  std::string _blank_b;   ///< blank space not yet filled by bar

  /**
  * @brief Display progress
  *
  * @param nbars Number of characters representing the current bar.
  */
  void display(T nbars)
  {
    const std::string & msg = progress<T>::_msg;
    if(nbars > _bar_len) nbars = _bar_len;

    _bar.assign(nbars, _bar_char);
    _blank_b.assign(_bar_len - nbars, ' ');

    printf("\r%s%s[%s%s]", msg.c_str(), _blank_a.c_str(), _bar.c_str(), _blank_b.c_str());
    fflush(stdout);
  }

  /**
  * @brief Evaluate if a new display is needed
  */
  void evaluate()
  {
    const T & num_next = progress<T>::_num_next;
    T prog = T(float(_counter) / float(num_next) * float(_bar_len));
    
    if(prog > _threshold)
    {
      _threshold = prog;
      this->display(prog);
    }
  }

  public:
  /**
  * @brief Constructor
  *
  * @param num_next     Estimate of how many times next() will be issued.
  * @param msg          Message to display together with progress bar.
  * @param bar_start    Number of characters after which the bar should start.
  * @param bar_len      Number of characters making up the bar.
  * @param bar_char     Character used to represent the bar.
  */
  progress_bar(T num_next, const char* msg, T bar_start = 60, T bar_len = 20, char bar_char = '=') : 
                  progress<T>(num_next, msg),
                  _counter(0),
                  _threshold(0),
                  _bar_len(bar_len),
                  _bar_char(bar_char)
  {
    int size = bar_start - progress<T>::_msg.size();
    if(size < 0) size = 0;

    _blank_a.assign(size, ' ');
    this->display(0);
  }

  /// increment progress counter by one
  void next()
  {
    _counter++;
    this->evaluate();
  }

  /// increment progress counter by a custom value
  void increment(T inc)
  {
    _counter += inc;
    this->evaluate();
  }

  /// finish up progress display
  void finish()
  {
    this->display(_bar_len);
    printf("\n");
  }
};

#endif

